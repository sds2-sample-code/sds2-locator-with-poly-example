import model

from Component import PrepareComponentForMember
from Designable.ProcessableComponent import ProcessableComponent
from MemberBase import GetMemberLink
from Point3D import Point3D
from .Screen import BuildComponentUI
from .Versions import LocatorSampleVersions
from .design import design_material
from kwargmixin.kwargmixin import KWArgMixin
from .locator import LocatorSampleLocator
from sds2.utility.gadget_protocol import GadgetComponent
from shape import Shape


@LocatorSampleVersions
class LocatorSample(GadgetComponent, ProcessableComponent, KWArgMixin):
    """
    Creates a plate out in space, based on the location chosen by the user
    """

    UserStr = "Locator Sample Component"
    DesignMaterial = design_material

    def __init__(self, **kwargs):
        LocatorSampleVersions.init_factory()(self, **kwargs) #  run the init from the
                                             #  versioning class
        ProcessableComponent.__init__(self, **kwargs)

        KWArgMixin.__init__(self, **kwargs)

    @property
    def Width(self):
        return self.HostDepth - (2 * self.HostFlangeThick)

    @property
    def Thickness(self):
        return .3

    @property
    def HostFlangeThick(self):
        return Shape(
            model.member(self.member_number).section_size
        ).FlangeThickness

    @property
    def HostWebThick(self):
        return Shape(
            model.member(self.member_number).section_size
        ).WebThickness

    @property
    def HostDepth(self):
        return Shape(model.member(self.member_number).section_size).Depth

    @property
    def HostXform(self):
        return GetMemberLink(self.member_number, False, False).GetXform()

    @property
    def Point1(self):
        return (
            self.GetReferencePoint() +
            -self.HostXform.GetBasisVectorY() *
            (self.Width / 2. - self.HostFlangeThick)
        )

    @property
    def Point2(self):
        return (
            self.GetReferencePoint() +
            self.HostXform.GetBasisVectorY() *
            (self.Width / 2. - self.HostFlangeThick)
        )

    def IsAllowedOnMember(self, mn):
        """Determine whether or not the component is allowed on the member

        This method will be called in modeling whenever a component add
        is started. It will control whether or not a member is allowed to be
        chosen; it will also determine which components are shown in the
        component add list if members are preselected.

        Args:
            mn (int): Member number of the member being considered

        Returns:
            bool: True if the component can be added to the member
                  False otherwise
        """

        return model.member(mn).member_type == model.Beam

    def SetReferencePointForMemberUI(self, mn):
        """Set the reference point for the component on the member

        The reference point is relative to the left end of the host member

        Args:
            mn (int): Member number of the member hosting the component
        """

        # see locator.py for implementation
        return LocatorSampleLocator().LocateReferencePoint(
            self,
            mn,
            design_method=design_material
        )

    def Add(self, mn):
        """Take care of any user input needed to add the component

        Args:
            mn (int): Member number of the member hosting the component

        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        # when producing a preview, proxies need member information
        # and at the point in the process, the component has not been filled
        # out with the necessary information
        # so, we pre-load the info with PrepareComponentForMember
        PrepareComponentForMember(self, mn)
        return self.SetReferencePointForMemberUI(mn)

    @classmethod
    def Factory(cls, host, ref_point):
        """Instantiate, initialize, and return an instance of the component

        Args:
            ref_pt (Point3D): reference point for the component
            host (int): Member number for the component host
            member_universe (list): List of members related to the component
        """

        comp = cls()
        PrepareComponentForMember(comp, host)
        comp.SetReferencePoint(ref_point)

        return comp

    # @staticmethod is required for this method
    # this is part of the Gadget Protocol interface
    @staticmethod
    def CreateCustomMultiEditableUI(model, gadget_factory):
        """Create the UI for the component

        Args:
            model (list): List of component objects considered for edit
            gadget_factory (GadgetFactory): Convenience class for creating
                                            items in the screen
        """
        BuildComponentUI(model, gadget_factory)

    def DesignForMember(self, mn):
        """Bulk of the work for adding the component is done here

        Any material should be added in this method

        Args:
            mn (int): Member number of the member hosting the component

        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """

        self.RegisterDesignProxy(self.DesignMaterial())
