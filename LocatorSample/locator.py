import copy

from Locator import Locator3DBase
from Polygon import PolygonBuilder
from Polygon import Preview
from Transform3D import MemberTransform
from sds2 import obj
from shape import Shape


class LocatorSampleLocator(Locator3DBase):
    # this is requred to properly clean up the polygon preview
    ############
    def __exit_preview(self):
        preview = getattr(self, '_preview', None)
        if preview:
            del self._preview
            preview.__exit__(None, None, None)

    def OnBestUnAnnotatePoint3D(self, best):
        self.__exit_preview
    ############

    def OnBestAnnotatePoint3D(self, best):
        """Access to the locator point while locating a point in the model

        Args:
            best (Point3D): The current locator point
        """
        try:
            # i prefer not to operate on the original where i can avoid it
            ref = copy.copy(best)
            self.global_to_host.TransformPoint(ref)
            self.component.SetReferencePoint(ref)

            if self.design_method:
                proxy = self.design_method(self.component)
                # proxies can generate polygons based on their attributes
                # we just need to grab them
                poly = next(proxy.global_polylist_iter())
                preview = Preview(poly)
                preview.__enter__()
                self._preview = preview
        except Exception as e:
            obj.warning_list(str(e))

    def LocateReferencePoint(self, component, host_number, design_method=None):
        """Locate the origin point for the component

        Args:
            component (LocatorSample): The component for which we are locating
            host_number (int): Member number for the member hosting the
                               component
            design_method (func, optional): Optional method that returns a
                                            proxy which can be used to generate
                                            polygons for the preview
        """

        host = obj.mb(host_number)
        host_to_global_xform = MemberTransform(
            host.left_location,
            host.right_location,
            host.rotation
        )

        global_to_host_xform = host_to_global_xform.Inverse()

        host_y = host_to_global_xform.GetBasisVectorY()
        host_z = host_to_global_xform.GetBasisVectorZ()

        host_shape = Shape(host.section_size)
        mid_translation = (
            (-host_y * host_shape.Depth/2.) +
            (host_z * host_shape.WebThickness)
        )

        host_left_ref = (host.left_location + mid_translation)

        host_right_ref = (host.right_location + mid_translation)

        self.SetAnchorGlobal(host.left_location)

        # here is were the locator pip is restricted
        #########################
        builder = PolygonBuilder()
        builder.addSide(host_left_ref)
        builder.addSide(host_right_ref)
        builder.addLine()
        self.SetCurrentSnapONPL(builder.get())
        #########################

        self.SetPrompt('Locate reference point')

        self.host_to_global = host_to_global_xform
        self.global_to_host = global_to_host_xform
        self.component = component
        self.design_method = design_method

        original_ref_point = component.ref_point
        original_design = component.material_creation_phases
        located_point = obj.pt()
        did_locate = self.AcquireGlobalPoint(located_point)

        # just some cleanup
        del self.host_to_global
        del self.global_to_host
        del self.component
        del self.design_method

        if did_locate:
            component.SetReferencePoint(
                global_to_host_xform.TransformPoint(located_point)
            )
        else:
            component.ref_point = original_ref_point
            component.material_creation_phases = original_design

        return did_locate
