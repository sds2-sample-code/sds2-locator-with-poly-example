from sds2.componentmetamorphoses import ComponentMetamorphoses
from metamorphoses import Version, Attr

class LocatorSampleVersions(ComponentMetamorphoses):
    versions = {}
    versions[0] = Version(
        Attr('x', False),
        Attr('y', 25.),
        Attr('z', "this is a string")
    )